#ifndef PROCESSOR_H
#define PROCESSOR_H

#include "node.h"
#include "rod.h"
#include <vector>

class Processor
{
private:
    int size;
    int pillar;

    double** A;
    double* delta;
    double* b;

    std::vector<Rod> rods;
    std::vector<Node> nodes;

    void setA();
    void setB();
    void setDelta();

public:
    Processor(const std::vector<Rod> rods, const std::vector<Node> nodes, int pillar);
    ~Processor();

    void gauss();

    double* getDelta();
    double** getA();
    double* getB();

    int getSize();
};

#endif // PROCESSOR_H
