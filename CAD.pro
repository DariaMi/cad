#-------------------------------------------------
#
# Project created by QtCreator 2016-11-11T01:11:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = CAD
TEMPLATE = app


SOURCES += main.cpp\
        preprocessor.cpp \
    functions.cpp \
    node.cpp \
    processor.cpp \
    rod.cpp \
    formnamesession.cpp \
    formdownloadsession.cpp \
    qcustomplot.cpp \
    formsavetoxml.cpp

HEADERS  += preprocessor.h \
    functions.h \
    node.h \
    processor.h \
    rod.h \
    formnamesession.h \
    formdownloadsession.h \
    qcustomplot.h \
    formsavetoxml.h

FORMS    += preprocessor.ui \
    formnamesession.ui \
    formdownloadsession.ui \
    formsavetoxml.ui

DISTFILES +=

RESOURCES += \
    resource.qrc

RESOURCES +=
RC_FILE = forIcon.rc
