#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "preprocessor.h"
#include <QLineEdit>
#include <QValidator>

QLineEdit* newLineEditNumbers(QWidget* parent = 0);
QLineEdit* newLineEditPositiveNumbers(QWidget* parent = 0);
QLineEdit* newLineEditNaturalNumbers(QWidget* parent = 0);
QRegExpValidator* newValidator();
#endif // FUNCTIONS_H
