#include "preprocessor.h"
#include "ui_preprocessor.h"
#include "functions.h"
#include "processor.h"
#include <math.h>
#include <QMessageBox>
#include <QComboBox>
#include <QDir>
#include <QDesktopServices>
#include <QUrl>
#include <QLineEdit>
#include <QGraphicsItem>
#include <QGraphicsView>
#include <QLabel>
#include <QSettings>
#include <QPushButton>
#include <QDebug>
#include <QVector>
#include <QIcon>
#include <QFile>
#include <QXmlStreamWriter>

Preprocessor::Preprocessor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Preprocessor)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);
    this->setWindowIcon(QIcon(":/Icon/Resource/Icons/CAD.png"));

    //Меню
    ui->actionSaveSession->setIcon(QIcon(":/Icon/Resource/Icons/save.png"));
    ui->actionLoadSession->setIcon(QIcon(":/Icon/Resource/Icons/load.png"));
    ui->actionExampleOfTasks->setIcon(QIcon(":/Icon/Resource/Icons/pdf.png"));
    ui->actionFormula->setIcon(QIcon(":/Icon/Resource/Icons/pdf.png"));
    ui->actionTechnicalTask->setIcon(QIcon(":/Icon/Resource/Icons/pdf.png"));
    ui->actionNames->setIcon(QIcon(":/Icon/Resource/Icons/information.png"));
    ui->actionDeveloper->setIcon(QIcon(":/Icon/Resource/Icons/girl.png"));

    //Первая вкладка
    ui->tabWidget->setCurrentIndex(0);
    ui->comboBox->setCurrentIndex(2);

    ui->rodTable->horizontalHeader()->setVisible(true);
    ui->rodTable->verticalHeader()->setVisible(true);

    isSave = false;
    pillar = ui->comboBox->currentIndex();

    for(int i = 0; i < 4; i++){
        ui->rodTable->setCellWidget(0,i,newLineEditPositiveNumbers(this));
    }
    ui->rodTable->setCellWidget(0, 4, newLineEditNumbers(this));

    ui->nodeTable->setCellWidget(0,0,newLineEditNaturalNumbers(this));
    ui->nodeTable->setCellWidget(0,1,newLineEditNumbers(this));

    //Вторая вкладка
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

    ui->zoomInButton->setIcon(QIcon(":/Icon/Resource/Icons/plus.png"));
    ui->zoomOutButton->setIcon(QIcon(":/Icon/Resource/Icons/minus.png"));

    //Третья вкладка
    ui->labelDelta->hide();
    ui->labelA->hide();
    ui->labelB->hide();

    ui->tableDelta->hide();
    ui->tableA->hide();
    ui->tableB->hide();

    ui->buttonShowA->setEnabled(false);
    ui->buttonHideA->setEnabled(false);
    ui->buttonShowB->setEnabled(false);
    ui->buttonHideB->setEnabled(false);

    //Вкладка "Графики"
    ui->widgetNxPlot->hide();
    ui->widgetSigmaXPlot->hide();
    ui->widgetExPlot->hide();
    ui->widgetUxPlot->hide();

    //Вкладка "Таблицы"
    ui->lineEditDivider->setValidator(new QRegExpValidator(QRegExp("[1-9]{1}[0-9]{0,4}")));
    ui->lineEditDivider->setEnabled(false);
    ui->pushButtonShowTables->setEnabled(false);

    ui->tableNx->hide();
    ui->tableSigmaX->hide();
    ui->tableEx->hide();
    ui->tableUx->hide();

    for(int i = 0; i < 2; i++){
        ui->tableNx->setColumnWidth(i,80);
        ui->tableSigmaX->setColumnWidth(i, 80);
        ui->tableEx->setColumnWidth(i, 80);
        ui->tableUx->setColumnWidth(i, 80);
    }

    ui->pushButtonSaveToFile->hide();
    formSaveToXml = new FormSaveToXml();
}

Preprocessor::~Preprocessor()
{
    QString filePath = qApp->applicationDirPath().append("/Formula.pdf");
    QFile::setPermissions(filePath, QFile::ReadOwner | QFile::WriteOwner);
    QFile::remove(filePath);

    filePath = qApp->applicationDirPath().append("/ExamplesOfTasks.pdf");
    QFile::setPermissions(filePath, QFile::ReadOwner | QFile::WriteOwner);
    QFile::remove(filePath);

    filePath = qApp->applicationDirPath().append("/TechnicalTask.pdf");
    QFile::setPermissions(filePath, QFile::ReadOwner | QFile::WriteOwner);
    QFile::remove(filePath);

    delete formSaveToXml;

    delete ui;
}

void Preprocessor::on_addRodButton_clicked()
{
    int newRow = ui->rodTable->rowCount();
    ui->rodTable->insertRow(newRow);

    for(int i = 0; i < 4; i++){
        ui->rodTable->setCellWidget(newRow,i,newLineEditPositiveNumbers(this));
    }
    ui->rodTable->setCellWidget(newRow, 4, newLineEditNumbers(this));
}

void Preprocessor::on_deleteRodButton_clicked()
{
    ui->rodTable->removeRow(ui->rodTable->rowCount()-1);
}

void Preprocessor::on_addNodeButton_clicked()
{
    int newRow = ui->nodeTable->rowCount();
    ui->nodeTable->insertRow(newRow);

    ui->nodeTable->setCellWidget(newRow,0,newLineEditNaturalNumbers(this));
    ui->nodeTable->setCellWidget(newRow,1,newLineEditNumbers(this));
}

void Preprocessor::on_deleteNodeButton_clicked()
{
    ui->nodeTable->removeRow(ui->nodeTable->rowCount()-1);
}

void Preprocessor::on_paintButton_clicked()
{
    if (isSave == false){
        QMessageBox::warning(this, "Изменения не были сохранены", "Сначала сохраните изменения.");
        return;
    }

    scene->clear();
    QPen pen(Qt::black);
    pen.setWidth(2);
    pen.setJoinStyle(Qt::MiterJoin);

    int widthRect = 200;    //длина пропорциональна L
    int heightRect = 70;    //высота пропорциональна A

    int startPointX = 0;
    int startPointY = 0;
    int symmetryAxis = startPointY + heightRect * 0.5;

    //Левая опора
    if (pillar == 0 || pillar == 2){
        drawLeftPillar(startPointX, startPointY, 70);
    }

    //Стержни
    for(unsigned int i = 0; i < rod.size(); i++){
        double kL = rod.at(i).getL() / rod.at(0).getL();
        widthRect = 200 * kL;

        double kA = rod.at(i).getA() / rod.at(0).getA();
        heightRect = 70 * kA;

        //Сам стержень
        scene->addRect(startPointX, symmetryAxis - heightRect * 0.5, widthRect, heightRect, pen);

        //Распределенная нагрузка
        drawQ(startPointX, symmetryAxis, widthRect, rod.at(i).getQ());
        if (rod.at(i).getQ() != 0){
            int value = rod.at(i).getQ();
            if (value < 0){
                value *= -1;
            }
            drawText(tr("%1").arg(value) + "Н/м", startPointX + widthRect * 0.4, symmetryAxis - 30, QColor(50, 190, 166));
        }

        //Сосредоточенная нагрузка
        for(unsigned int j = 0; j < node.size(); j++){
            if (node.at(j).getNumber() == i+1){
                drawF(startPointX, symmetryAxis, node.at(j).getForce());
                if (node.at(j).getForce() > 0){
                    drawText(tr("%1").arg(node.at(j).getForce()) + "Н", startPointX, symmetryAxis - 30, QColor(224, 79, 95));
                }
                else if(node.at(j).getForce() < 0){
                    drawText(tr("%1").arg(node.at(j).getForce()*(-1)) + "Н", startPointX - 30, symmetryAxis + 3, QColor(224, 79, 95));
                }
            }
        }
        startPointX += widthRect;
    }

    //Последняя сосредоточенная нагрузка
    for(unsigned int j = 0; j < node.size(); j++){
        if (node.at(j).getNumber() == rod.size() + 1){
            drawF(startPointX, symmetryAxis, node.at(j).getForce());
            if (node.at(j).getForce() > 0){
                drawText(tr("%1").arg(node.at(j).getForce()) + "Н", startPointX, symmetryAxis - 30, QColor(224, 79, 95));
            }
            else if(node.at(j).getForce() < 0){
                drawText(tr("%1").arg(node.at(j).getForce()*(-1)) + "Н", startPointX - 30, symmetryAxis + 3, QColor(224, 79, 95));
            }
        }
    }

    //Правая опора
    if (pillar == 1 || pillar == 2){
        drawRightPillar(startPointX, startPointY, 70);
    }


    //Кнопочка
    ui->paintButton->setEnabled(false);
}

void Preprocessor::on_saveChangesButton_clicked()
{
    if (isInputCorrectForRodes() == false || isInputCorrectForNodes() == false)
        return;

    int rowCount = ui->rodTable->rowCount();
    rod.clear();
    rod.resize(rowCount);
    for(int i = 0; i < rowCount; i++){
        this->rod.at(i).setL(((QLineEdit*)ui->rodTable->cellWidget(i,0))->text().toDouble());
        this->rod.at(i).setE(((QLineEdit*)ui->rodTable->cellWidget(i,1))->text().toDouble());
        this->rod.at(i).setA(((QLineEdit*)ui->rodTable->cellWidget(i,2))->text().toDouble());
        this->rod.at(i).setSigma(((QLineEdit*)ui->rodTable->cellWidget(i,3))->text().toDouble());
        this->rod.at(i).setQ(((QLineEdit*)ui->rodTable->cellWidget(i,4))->text().toDouble());
    }

    rowCount = ui->nodeTable->rowCount();
    node.clear();
    node.resize(rowCount);
    for(int i = 0; i < rowCount; i++){
        this->node.at(i).setNumber(((QLineEdit*)ui->nodeTable->cellWidget(i,0))->text().toDouble());
        this->node.at(i).setForce(((QLineEdit*)ui->nodeTable->cellWidget(i,1))->text().toDouble());
    }

    pillar = ui->comboBox->currentIndex();

    isSave = true;
    QMessageBox::information(this, "Сохранено", "Изменения сохранены.");

    //Для второй вкладки
    scene->clear();
    ui->paintButton->setEnabled(true);

    //Для третьей вкладки
    ui->labelDelta->hide();
    ui->labelA->hide();
    ui->labelB->hide();

    ui->tableDelta->hide();
    ui->tableA->hide();
    ui->tableB->hide();

    ui->buttonShowA->setEnabled(false);
    ui->buttonHideA->setEnabled(false);
    ui->buttonShowB->setEnabled(false);
    ui->buttonHideB->setEnabled(false);

    ui->toCalculateButton->setEnabled(true);

    //Для вкладки "Графики"
    ui->widgetNxPlot->hide();
    ui->widgetSigmaXPlot->hide();
    ui->widgetExPlot->hide();
    ui->widgetUxPlot->hide();

    //Для вкладки "Таблицы"
    ui->lineEditDivider->clear();
    ui->lineEditDivider->setEnabled(false);

    ui->tableNx->hide();
    ui->tableSigmaX->hide();
    ui->tableEx->hide();
    ui->tableUx->hide();

    ui->pushButtonSaveToFile->hide();
}

void Preprocessor::on_actionNames_triggered()
{
    QMessageBox names;
    names.setText("L - длина стержня [м] \n"
                  "E - модуль Юнга [Па] = [Н / м^2] \n"
                  "A - площадь поперечного сечения [м^2] \n"
                  "[σ] - максимально допускаемое напряжение [Па] \n"
                  "q - значение распределенной нагрузки [Н / м] \n"
                  "F - значение сосредоточенной силы [Н] \n"
                  "\nДля таблиц: \n"
                  "N(x) - продольная сила [Н] \n"
                  "σ(x) - напряжение [Па] \n"
                  "Ԑ(x) - деформация [÷ 10^6] \n"
                  "U(x) - перемещение [мкм]");
    names.setWindowTitle("Обозначения");
    names.setIconPixmap(QPixmap(":/Icon/Resource/Icons/cube.png"));
    names.setWindowIcon(QIcon(":/Icon/Resource/Icons/CAD.png"));
    names.exec();
}

void Preprocessor::on_actionTechnicalTask_triggered()
{
    QFile file(":/Document/Resource/Documents/TechnicalTask.pdf");;
    file.copy(qApp->applicationDirPath().append("/TechnicalTask.pdf"));
    QDesktopServices::openUrl(QUrl::fromLocalFile(qApp->applicationDirPath().append("/TechnicalTask.pdf")));
}

void Preprocessor::on_actionExampleOfTasks_triggered()
{
    QFile file(":/Document/Resource/Documents/ExamplesOfTasks.pdf");;
    file.copy(qApp->applicationDirPath().append("/ExamplesOfTasks.pdf"));
    QDesktopServices::openUrl(QUrl::fromLocalFile(qApp->applicationDirPath().append("/ExamplesOfTasks.pdf")));
}

void Preprocessor::on_actionFormula_triggered()
{
    QFile file(":/Document/Resource/Documents/Formula.pdf");;
    file.copy(qApp->applicationDirPath().append("/Formula.pdf"));
    QDesktopServices::openUrl(QUrl::fromLocalFile(qApp->applicationDirPath().append("/Formula.pdf")));
}

bool Preprocessor::isInputCorrectForRodes()
{
    int rowCount = ui->rodTable->rowCount();
    int columnCount = ui->rodTable->columnCount();

    if (rowCount == 0){
        QMessageBox::warning(this, "Пусто", "Нет ни одного стержня.\nДолжен быть хотя бы один.");
        return false;
    }

    for (int i = 0; i < rowCount; i++){
        for (int j = 0; j < columnCount; j++){
            if (((QLineEdit*)ui->rodTable->cellWidget(i,j))->text() == "" ){
                QMessageBox::warning(this, "Пусто", "Не все ячейки таблицы стержней заполнены. \nЗаполните их и попробуйте снова.");
                return false;
            }
        }

        for (int j = 0; j < columnCount-1; j++){
            if (((QLineEdit*)ui->rodTable->cellWidget(i,j))->text().toDouble() == 0){
                QMessageBox::warning(this, "Неверное значение", "L, E, A или σ не могут равняться нулю.");
                return false;
            }
        }
    }

    return true;
}

bool Preprocessor::isInputCorrectForNodes()
{
    int rowCount = ui->nodeTable->rowCount();
    int columnCount = ui->nodeTable->columnCount();

    for (int i = 0; i < rowCount; i++){
        for (int j = 0; j < columnCount; j++){
            if (((QLineEdit*)ui->nodeTable->cellWidget(i,j))->text() == "" ){
                QMessageBox::warning(this, "Пусто", "Не все ячейки таблицы узлов заполнены. \nЗаполните их и попробуйте снова.");
                return false;
            }
        }

        if (((QLineEdit*)ui->nodeTable->cellWidget(i,0))->text().toDouble() > ui->rodTable->rowCount() + 1){
            QMessageBox::warning(this, "Неверное значение", "Узла с таким номером нет.");
            return false;
        }
    }

    return true;
}

void Preprocessor::wheelEvent(QWheelEvent *event)
{
    const double scaleFactor = 1.15;
    if(event->delta() > 0){
        ui->graphicsView->scale(scaleFactor, scaleFactor);      // Zoom in
    }
    else{
        ui->graphicsView->scale(1.0 / scaleFactor, 1.0 / scaleFactor);      // Zooming out
    }
}

void Preprocessor::on_zoomInButton_clicked()
{
    const double scaleFactor = 1.15;
    ui->graphicsView->scale(scaleFactor, scaleFactor);
}

void Preprocessor::on_zoomOutButton_clicked()
{
    const double scaleFactor = 1.15;
    ui->graphicsView->scale(1.0 / scaleFactor, 1.0 / scaleFactor);
}

void Preprocessor::drawLeftPillar(int startPointX, int startPointY, int length)
{
    QPen pen(Qt::black);
    pen.setWidth(2);

    scene->addLine(startPointX, startPointY, startPointX, startPointY + length, pen);
    for(int i = startPointY; i < startPointY + length; i += 10){
        int height = i + 10;
        scene->addLine(startPointX, i, startPointX - 10, height, pen);
    }
}

void Preprocessor::drawRightPillar(int startPointX, int startPointY, int length)
{
    QPen pen(Qt::black);
    pen.setWidth(2);

    scene->addLine(startPointX, startPointY, startPointX, startPointY + length, pen);
    for(int i = startPointY + 10; i <= startPointY + length; i += 10){
        int height = i - 10;
        scene->addLine(startPointX, i, startPointX + 10, height, pen);
    }
}

void Preprocessor::drawQ(int startPointX, int startPointY, int length, double q)
{
    if (q == 0)
        return;

    QPen pen(QColor(50, 190, 166));
    //QPen pen(QColor(26, 17, 132));
    pen.setWidth(2);

    scene->addLine(startPointX, startPointY, startPointX + length, startPointY, pen);

    if (q > 0){
        for(int i = startPointX + 15; i <= startPointX + length; i += 15){
            scene->addLine(i, startPointY, i - 5, startPointY - 5, pen);
            scene->addLine(i, startPointY, i - 5, startPointY + 5, pen);
        }
    }
    else{
        for(int i = startPointX + length - 10; i >= startPointX; i -= 15){
            scene->addLine(i, startPointY, i + 5, startPointY - 5, pen);
            scene->addLine(i, startPointY, i + 5, startPointY + 5, pen);
        }
    }
}

void Preprocessor::drawF(int startPointX, int startPointY, double F)
{
    if(F == 0)
        return;

    //QPen pen(QColor(254, 1, 0));
    QPen pen(QColor(224, 79, 95));
    pen.setWidth(2);
    scene->addLine(startPointX, startPointY, (F > 0) ? (startPointX + 20) : (startPointX - 20), startPointY, pen);

    if(F > 0){
        scene->addLine(startPointX + 20, startPointY, startPointX + 15, startPointY - 5, pen);
        scene->addLine(startPointX + 20, startPointY, startPointX + 15, startPointY + 5, pen);
    }
    else{
        scene->addLine(startPointX - 20, startPointY, startPointX - 15, startPointY - 5, pen);
        scene->addLine(startPointX - 20, startPointY, startPointX - 15, startPointY + 5, pen);
    }
}

void Preprocessor::drawText(QString text, int posX, int posY, QColor textColor)
{
    QGraphicsTextItem* item = new QGraphicsTextItem;

    QFont font;
    font.setFamily("Times New Roman");
    font.setPixelSize(14);
    font.setItalic(true);

    item->setPos(posX, posY);
    item->setFont(font);
    item->setPlainText(text);
    item->setDefaultTextColor(textColor);

    scene->addItem(item);
}

void Preprocessor::drawPlots()
{
    if (isSave == false){
        QMessageBox::warning(this, "Изменения не были сохранены", "Сначала сохраните изменения.");
        return;
    }

    // Nx = EA/L * (delta(L) - delta(0))  +  qL/2 * (1 - 2*x/L)

    QVector<double> x(this->rod.size() * 2, 0);
    for (unsigned int i = 0; i < this->rod.size(); i++){
        x[i*2 + 1] = this->rod.at(i).getL();
    }

    QVector<double> Nx(this->rod.size() * 2, 0);
    for(unsigned int i = 0; i < this->rod.size(); i++){
        double E = this->rod.at(i).getE();
        double A = this->rod.at(i).getA();
        double L = this->rod.at(i).getL();
        double q = this->rod.at(i).getQ();

        double deltaL = this->delta.at(i+1);
        double delta0 = this->delta.at(i);

        Nx[i*2 + 1] = E*A/L * (deltaL - delta0) + q*L*0.5 * (1 - 2 * x[i*2 + 1] / L);
        Nx[i*2]     = E*A/L * (deltaL - delta0) + q*L*0.5 * (1 - 2 * x[i*2] / L);
    }

    QVector<double> L(this->rod.size() * 2, 0);
    double l = 0;
    for (unsigned int i = 0; i < this->rod.size(); i++){
        l += this->rod.at(i).getL();
        L[i*2 + 1] = l;
    }
    for (int i = 1; i < L.size() - 1; i += 2){
        L[i+1] = L[i];
    }

    //Строим график N(x)
    ui->widgetNxPlot->clearGraphs();

    ui->widgetNxPlot->addGraph();
    ui->widgetNxPlot->graph(0)->setData(L, Nx, true);
    ui->widgetNxPlot->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 20)));
    ui->widgetNxPlot->graph(0)->setPen(QPen(QBrush(QColor(0, 0, 255)), 2));

    ui->widgetNxPlot->xAxis->setLabel("L [м]");
    ui->widgetNxPlot->yAxis->setLabel("N(x) [Н]");
    ui->widgetNxPlot->xAxis->setLabelFont(QFont("Georgia", 10));
    ui->widgetNxPlot->yAxis->setLabelFont(QFont("Georgia", 10));

    double minY = Nx[0];
    double maxY = Nx[0];
    for (int i = 0; i < Nx.size(); i++){
        if (Nx[i] < minY)
            minY = Nx[i];
        else if (Nx[i] > maxY)
            maxY = Nx[i];
    }
    ui->widgetNxPlot->xAxis->setRange(0, l);
    ui->widgetNxPlot->yAxis->setRange(minY, maxY);

    ui->widgetNxPlot->replot();
    ui->widgetNxPlot->show();

    //Для графика SigmaX
    //SigmaX = N(x) / Ax
    QVector<double> SigmaX = Nx;
    for(unsigned int i = 0; i < this->rod.size(); i++){
        double A = this->rod.at(i).getA();

        SigmaX[i*2]     /= A;
        SigmaX[i*2 + 1] /= A;
    }

    //Строим SigmaX
    ui->widgetSigmaXPlot->clearGraphs();

    ui->widgetSigmaXPlot->addGraph();
    ui->widgetSigmaXPlot->graph(0)->setData(L, SigmaX, true);
    ui->widgetSigmaXPlot->graph(0)->setBrush(QBrush(QColor(255, 255, 0, 50)));
    ui->widgetSigmaXPlot->graph(0)->setPen(QPen(QBrush(QColor(255, 255, 0)), 2));

    ui->widgetSigmaXPlot->xAxis->setLabel("L [м]");
    ui->widgetSigmaXPlot->yAxis->setLabel("σ(x) [Па]");
    ui->widgetSigmaXPlot->xAxis->setLabelFont(QFont("Georgia", 10));
    ui->widgetSigmaXPlot->yAxis->setLabelFont(QFont("Georgia", 10));

    minY = SigmaX[0];
    maxY = SigmaX[0];
    for (int i = 0; i < SigmaX.size(); i++){
        if (SigmaX[i] < minY)
            minY = SigmaX[i];
        else if (SigmaX[i] > maxY)
            maxY = SigmaX[i];
    }
    ui->widgetSigmaXPlot->xAxis->setRange(0,l);
    ui->widgetSigmaXPlot->yAxis->setRange(minY, maxY);

    ui->widgetSigmaXPlot->replot();
    ui->widgetSigmaXPlot->show();

    //Для графика Ԑ(x)
    //Ԑ(x) = σ(x) / Ex
    QVector<double> Ex = SigmaX;
    for(unsigned int i = 0; i < this->rod.size(); i++){
        double E = this->rod.at(i).getE();

        Ex[i*2]     /= E;
        Ex[i*2 + 1] /= E;
    }

    //Строим Ԑ(x)
    ui->widgetExPlot->clearGraphs();

    ui->widgetExPlot->addGraph();
    ui->widgetExPlot->graph(0)->setData(L, Ex, true);
    ui->widgetExPlot->graph(0)->setBrush(QBrush(QColor(0, 100, 0, 40)));
    ui->widgetExPlot->graph(0)->setPen(QPen(QBrush(QColor(0, 100, 0)), 2));

    ui->widgetExPlot->xAxis->setLabel("L [м]");
    ui->widgetExPlot->yAxis->setLabel("Ԑ(x) * 10^(-6)");
    ui->widgetExPlot->xAxis->setLabelFont(QFont("Georgia", 10));
    ui->widgetExPlot->yAxis->setLabelFont(QFont("Georgia", 10));

    minY = Ex[0];
    maxY = Ex[0];
    for (int i = 0; i < Ex.size(); i++){
        if (Ex[i] < minY)
            minY = Ex[i];
        else if (Ex[i] > maxY)
            maxY = Ex[i];
    }
    ui->widgetExPlot->xAxis->setRange(0,l);
    ui->widgetExPlot->yAxis->setRange(minY, maxY);

    ui->widgetExPlot->replot();
    ui->widgetExPlot->show();


    //Для графика U(x)
    //U(x) = U(0) + x/L * (U(L) - U(0)) + q*(L^2)/(2*E*A) * x/L * (1-x/L)
    //U(0), U(L) = соответствующим delta

    unsigned short int divider = 30; //Количество отрезков, на которые делим локальный стержень

    QVector<double> Ux(rod.size()*divider + 1);
    QVector<double> globalL(Ux.size());
    globalL[0] = 0;
    Ux[0] = delta[0];

    double gl = 0;
    minY = Ux[0];
    maxY = Ux[0];

    for(unsigned int i = 0; i < this->rod.size(); i++){
        double E = this->rod.at(i).getE();
        double A = this->rod.at(i).getA();
        double L = this->rod.at(i).getL();
        double q = this->rod.at(i).getQ();

        double deltaL = this->delta.at(i+1);
        double delta0 = this->delta.at(i);

        double step = L / divider; //шаг
        gl += step;
        double x = step;

        for(int j = 1; j < divider; j++){
            int index = i*divider + j;
            globalL[index] = gl;
            gl += step;

            Ux[index] = delta0 + x/L * (deltaL - delta0) + q*L*L/(2*E*A) * x/L *(1-x/L);
            x += step;

            if (Ux[index] < minY)
                minY = Ux[index];
            else if (Ux[index] > maxY)
                maxY = Ux[index];
        }

        int index = (i+1)*divider;
        globalL[index] = gl;
        Ux[index] = deltaL;

        if (Ux[index] < minY)
            minY = Ux[index];
        else if (Ux[index] > maxY)
            maxY = Ux[index];
    }

    //Строим U(x)
    ui->widgetUxPlot->clearGraphs();

    ui->widgetUxPlot->addGraph();
    ui->widgetUxPlot->graph(0)->setData(globalL, Ux, true);
    ui->widgetUxPlot->graph(0)->setBrush(QBrush(QColor(230, 0, 0, 100)));
    ui->widgetUxPlot->graph(0)->setPen(QPen(QBrush(QColor(230, 0, 0)), 2));

    ui->widgetUxPlot->xAxis->setLabel("L [м]");
    ui->widgetUxPlot->yAxis->setLabel("U(x) [мкм]");
    ui->widgetUxPlot->xAxis->setLabelFont(QFont("Georgia", 10));
    ui->widgetUxPlot->yAxis->setLabelFont(QFont("Georgia", 10));

    ui->widgetUxPlot->xAxis->setRange(0,gl);
    ui->widgetUxPlot->yAxis->setRange(minY, maxY);

    ui->widgetUxPlot->replot();
    ui->widgetUxPlot->show();
}

void Preprocessor::on_toCalculateButton_clicked()
{
    if (isSave == false){
        QMessageBox::warning(this, "Изменения не были сохранены", "Сначала сохраните изменения.");
        return;
    }

    ui->toCalculateButton->setEnabled(false);

    ui->tableDelta->clearContents();
    ui->tableA->clearContents();
    ui->tableB->clearContents();

    Processor p (this->rod, this->node, this->pillar);

    ui->labelDelta->show();
    ui->tableDelta->show();
    ui->labelA->show();
    ui->tableA->show();
    ui->labelB->show();
    ui->tableB->show();

    double** A = p.getA();
    double* b = p.getB();

    //Для таблицы A
    ui->tableA->setRowCount(this->rod.size()+1);
    ui->tableA->setColumnCount(this->rod.size()+1);
    for (int i = 0; i < ui->tableA->rowCount(); i++){
        for (int j = 0; j < ui->tableA->columnCount(); j++){
            ui->tableA->setCellWidget(i, j, newLineEditNumbers(this));
            ((QLineEdit*)ui->tableA->cellWidget(i, j))->setReadOnly(true);
            ((QLineEdit*)ui->tableA->cellWidget(i, j))->setText(QString::number(A[i][j], 'f', 6));
        }
    }

    //Для таблицы B
    ui->tableB->setRowCount(this->rod.size()+1);
    for (int i = 0; i < ui->tableB->rowCount(); i++){
        ui->tableB->setCellWidget(i, 0, newLineEditNumbers(this));
        ((QLineEdit*)ui->tableB->cellWidget(i, 0))->setReadOnly(true);
        ((QLineEdit*)ui->tableB->cellWidget(i, 0))->setText(QString::number(b[i], 'f', 6));
    }


    p.gauss();

    double* delta = p.getDelta();

    this->delta.clear();
    for (int i = 0; i < p.getSize(); i++){
        this->delta.push_back(delta[i]);
    }

    //Для таблицы Delta
    ui->tableDelta->setRowCount(this->rod.size()+1);
    for (unsigned int i = 0; i < this->rod.size()+1; i++){
        ui->tableDelta->setCellWidget(i, 0, newLineEditNumbers(this));
        ((QLineEdit*)ui->tableDelta->cellWidget(i, 0))->setReadOnly(true);
        ((QLineEdit*)ui->tableDelta->cellWidget(i, 0))->setText(QString::number(delta[i], 'f', 6));
    }

    //Кнопочки
    ui->buttonShowA->setEnabled(true);
    ui->buttonHideA->setEnabled(true);
    ui->buttonShowB->setEnabled(true);
    ui->buttonHideB->setEnabled(true);

    ui->pushButtonBuildPlots->setEnabled(true);
    ui->lineEditDivider->setEnabled(true);
}

void Preprocessor::on_buttonShowA_clicked()
{
    ui->labelA->show();
    ui->tableA->show();
}

void Preprocessor::on_buttonHideA_clicked()
{
    ui->labelA->hide();
    ui->tableA->hide();
}

void Preprocessor::on_buttonShowB_clicked()
{
    ui->labelB->show();
    ui->tableB->show();
}

void Preprocessor::on_buttonHideB_clicked()
{
    ui->labelB->hide();
    ui->tableB->hide();
}

void Preprocessor::on_actionSaveSession_triggered()
{
    if (isSave == false){
        QMessageBox::warning(this, "Изменения не были сохранены", "Сначала сохраните изменения.");
        return;
    }

    formNameSession = new FormNameSession();
    formNameSession->show();
    QObject::connect(formNameSession, SIGNAL(save()), this, SLOT(saveSession()));
    QObject::connect(formNameSession, SIGNAL(cancel()), this, SLOT(deleteFormNameSession()));
}

void Preprocessor::saveSession()
{
    QSettings* settings = new QSettings(QDir::toNativeSeparators(qApp->applicationDirPath() + "/" + formNameSession->getName() + ".ini"), QSettings::IniFormat);

    if(settings->contains("isSave")){
        QMessageBox qmb("Имя занято",
                        "Сеанс с таким именем уже существует.\nПерезаписать?",
                        QMessageBox::Question,
                        QMessageBox::Yes,
                        QMessageBox::No,
                        QMessageBox::NoButton);
        qmb.setButtonText(QMessageBox::Yes, "Да");
        qmb.setButtonText(QMessageBox::No, "Нет");
        qmb.setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
        if(qmb.exec() != QMessageBox::Yes){
            deleteFormNameSession();
            return;
        }
    }

    settings->clear();
    settings->setValue("isSave", isSave);
    settings->setValue("pillar", pillar);

    settings->beginGroup("/Rods");
    settings->setValue("size", rod.size());
    for(unsigned int i = 0; i < rod.size(); i++){
        settings->beginGroup("/Rod" + QString::number(i+1));
        settings->setValue("A", rod.at(i).getA());
        settings->setValue("E", rod.at(i).getE());
        settings->setValue("L", rod.at(i).getL());
        settings->setValue("sigma", rod.at(i).getSigma());
        settings->setValue("q", rod.at(i).getQ());
        settings->endGroup();
    }
    settings->endGroup();

    settings->beginGroup("/Nodes");
    settings->setValue("size", node.size());
    for(unsigned int i = 0; i < node.size(); i++){
        settings->beginGroup("/Node" + QString::number(i+1));
        settings->setValue("number", node.at(i).getNumber());
        settings->setValue("force", node.at(i).getForce());
        settings->endGroup();
    }
    settings->endGroup();

    deleteFormNameSession();

    QMessageBox::information(this, "Сохранено", "Сеанс успешно сохранен.");
}

void Preprocessor::deleteFormNameSession()
{
    formNameSession->~FormNameSession();
}

void Preprocessor::on_actionLoadSession_triggered()
{
    formDownloadSession = new FormDownloadSession();
    formDownloadSession->show();
    QObject::connect(formDownloadSession, SIGNAL(download()), this, SLOT(downloadSession()));
    QObject::connect(formDownloadSession, SIGNAL(cancel()), this, SLOT(deleteFormDownloadSession()));
}

void Preprocessor::downloadSession()
{
    //Загрузка в память
    QSettings* settings = new QSettings(QDir::toNativeSeparators(qApp->applicationDirPath() + "/" + formDownloadSession->getName() + ".ini"), QSettings::IniFormat);

    if(!settings->contains("isSave")){
        QMessageBox::warning(this,"Неверное имя","Сеанса с таким именем не существует.");
        deleteFormDownloadSession();
        return;
    }

    isSave = settings->value("isSave").toBool();
    pillar = settings->value("pillar").toInt();

    settings->beginGroup("/Rods");    
    int n = settings->value("size").toInt();
    rod.clear();
    rod.resize(n);
    for(int i = 0; i < n; i++){
        settings->beginGroup("/Rod" + QString::number(i+1));
        this->rod.at(i).setA(settings->value("A").toDouble());
        this->rod.at(i).setE(settings->value("E").toDouble());
        this->rod.at(i).setL(settings->value("L").toDouble());
        this->rod.at(i).setSigma(settings->value("sigma").toDouble());
        this->rod.at(i).setQ(settings->value("q").toDouble());
        settings->endGroup();
    }
    settings->endGroup();

    settings->beginGroup("/Nodes");
    n = settings->value("size").toInt();
    node.clear();
    node.resize(n);
    for(int i = 0; i < n; i++){
        settings->beginGroup("/Node" + QString::number(i+1));
        this->node.at(i).setNumber(settings->value("number").toInt());
        this->node.at(i).setForce(settings->value("force").toDouble());
        settings->endGroup();
    }
    settings->endGroup();


    //Загрузка в таблицы
    ui->rodTable->clearContents();
    int rowCount = this->rod.size();
    ui->rodTable->setRowCount(rowCount);

    for(int i = 0; i < rowCount; i++){
        for(int j = 0; j < 4; j++){
            ui->rodTable->setCellWidget(i,j,newLineEditPositiveNumbers(this));
        }
        ui->rodTable->setCellWidget(i, 4, newLineEditNumbers(this));

        ((QLineEdit*)ui->rodTable->cellWidget(i,0))->setText(QString::number(this->rod.at(i).getL(), 'f', 6));
        ((QLineEdit*)ui->rodTable->cellWidget(i,1))->setText(QString::number(this->rod.at(i).getE(), 'f', 6));
        ((QLineEdit*)ui->rodTable->cellWidget(i,2))->setText(QString::number(this->rod.at(i).getA(), 'f', 6));
        ((QLineEdit*)ui->rodTable->cellWidget(i,3))->setText(QString::number(this->rod.at(i).getSigma(), 'f', 6));
        ((QLineEdit*)ui->rodTable->cellWidget(i,4))->setText(QString::number(this->rod.at(i).getQ(), 'f', 6));
    }

    ui->nodeTable->clearContents();
    rowCount = this->node.size();
    ui->nodeTable->setRowCount(rowCount);

    for(int i = 0; i < rowCount; i++){
        ui->nodeTable->setCellWidget(i,0,newLineEditNaturalNumbers(this));
        ui->nodeTable->setCellWidget(i,1,newLineEditNumbers(this));

        ((QLineEdit*)ui->nodeTable->cellWidget(i,0))->setText(QString::number(this->node.at(i).getNumber()));
        ((QLineEdit*)ui->nodeTable->cellWidget(i,1))->setText(QString::number(this->node.at(i).getForce(), 'f', 6));
    }

    ui->comboBox->setCurrentIndex(pillar);

    //Отрисовка конструкции
    on_paintButton_clicked();

    //Рассчет конструкции
    on_toCalculateButton_clicked();

    //Построение графиков
    drawPlots();

    //Кнопочки
    ui->paintButton->setEnabled(false);
    ui->toCalculateButton->setEnabled(false);
    ui->pushButtonBuildPlots->setEnabled(false);

    ui->lineEditDivider->clear();
    ui->tableEx->hide();
    ui->tableNx->hide();
    ui->tableUx->hide();
    ui->tableSigmaX->hide();

    deleteFormDownloadSession();

    QMessageBox::information(this, "Загружено", "Сеанс успешно загружен.");
}

void Preprocessor::deleteFormDownloadSession()
{
    formDownloadSession->~FormDownloadSession();
}

void Preprocessor::on_pushButtonBuildPlots_clicked()
{
    drawPlots();

    //Кнопочка
    ui->pushButtonBuildPlots->setEnabled(false);
}

void Preprocessor::showTables(){
    ui->pushButtonShowTables->setEnabled(false);

    ui->tableNx->setRowCount(0);
    ui->tableSigmaX->setRowCount(0);
    ui->tableEx->setRowCount(0);
    ui->tableUx->setRowCount(0);

    unsigned short int divider = ui->lineEditDivider->text().toInt();

    double globalL = 0;

    for(unsigned int i = 0; i < this->rod.size(); i++){
        double E = this->rod.at(i).getE();
        double A = this->rod.at(i).getA();
        double L = this->rod.at(i).getL();
        double q = this->rod.at(i).getQ();

        double deltaL = this->delta.at(i+1);
        double delta0 = this->delta.at(i);

        double step = L / divider;

        if(ui->tableNx->rowCount() != 0){
            double firstN = (E*A/L * (deltaL - delta0)  +  q*L/2);
            double firstSigma = firstN / A;
            double firstE = firstSigma / E;

            if(firstN != ui->tableNx->item(ui->tableNx->rowCount()-1, 1)->text().toDouble()){
                this->helpTo_showTables(ui->tableNx, globalL, firstN);
            }
            if(firstSigma != ui->tableSigmaX->item(ui->tableSigmaX->rowCount()-1, 1)->text().toDouble()){
                this->helpTo_showTables(ui->tableSigmaX, globalL, firstSigma);
                if(firstSigma > this->rod.at(i).getSigma() * pow(10,6)){
                    ui->tableSigmaX->item(ui->tableSigmaX->rowCount()-1, 1)->setBackgroundColor(QColor(255,251,179));
                }
            }
            if(firstE != ui->tableEx->item(ui->tableEx->rowCount()-1, 1)->text().toDouble()){
                this->helpTo_showTables(ui->tableEx, globalL, firstE);
            }
        }

        double x = 0;
        if (ui->tableNx->rowCount() != 0){
            x += step;
            globalL += step;
        }

        for (; x <= L; x += step){
            double Nx = E*A/L * (deltaL - delta0)  +  q*L/2 * (1 - 2*x/L);
            double SigmaX = Nx / A;
            double Ex = SigmaX / E;
            double Ux = delta0 + x/L * (deltaL - delta0) + q*L/(2*E*A) * x * (1-x/L);

            this->helpTo_showTables(ui->tableNx, globalL, Nx);
            this->helpTo_showTables(ui->tableSigmaX, globalL, SigmaX);
            if(SigmaX > this->rod.at(i).getSigma() * pow(10,6)){
                ui->tableSigmaX->item(ui->tableSigmaX->rowCount()-1, 1)->setBackgroundColor(QColor(255,251,179));
            }
            this->helpTo_showTables(ui->tableEx, globalL, Ex);
            this->helpTo_showTables(ui->tableUx, globalL, Ux);

            globalL += step;
        }
        globalL -= step;
    }

    ui->tableNx->show();
    ui->tableSigmaX->show();
    ui->tableEx->show();
    ui->tableUx->show();
    ui->pushButtonSaveToFile->show();
}

void Preprocessor::helpTo_showTables(QTableWidget* table, double item1, double item2){
    table->insertRow(table->rowCount());
    table->setItem(table->rowCount()-1, 0, new QTableWidgetItem(QString::number(item1, 'f', 6)));
    table->setItem(table->rowCount()-1, 1, new QTableWidgetItem(QString::number(item2, 'f', 6)));

    for(int i = 0; i < table->columnCount(); i++){
        table->item(table->rowCount()-1, i)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    }
}

void Preprocessor::on_pushButtonShowTables_clicked()
{
    this->showTables();
}

void Preprocessor::on_lineEditDivider_textChanged(const QString &arg1)
{
    if(arg1=="")
        ui->pushButtonShowTables->setEnabled(false);
    else
        ui->pushButtonShowTables->setEnabled(true);
}

void Preprocessor::on_actionDeveloper_triggered()
{
    QMessageBox developer;
    developer.setText("Михайлова Дарья Юрьевна\nМГТУ «Станкин»\n2016 год");
    developer.setWindowTitle("Разработчик");
    developer.setIconPixmap(QPixmap(":/Icon/Resource/Icons/girl.png"));
    developer.setWindowIcon(QIcon(":/Icon/Resource/Icons/CAD.png"));
    developer.exec();
}

void Preprocessor::on_pushButtonSaveToFile_clicked()
{
    formSaveToXml->show();
    QObject::connect(formSaveToXml, SIGNAL(save(QString)), this, SLOT(createXml(QString)));
}

void Preprocessor::createXml(QString fileName)
{
    QFile file(fileName);
    file.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("Tables");

    xmlWriter.writeStartElement("NxTable");
    xmlWriter.writeComment("N [N] - longitudinal force");
    int rowCount = ui->tableNx->rowCount();
    for (int i = 0; i < rowCount; i++){
        xmlWriter.writeStartElement("Nx");
            xmlWriter.writeAttribute("x", ui->tableNx->item(i,0)->text());
            xmlWriter.writeAttribute("N", ui->tableNx->item(i,1)->text());
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement("SigmaXTable");
    xmlWriter.writeComment("Sigma [Pa] - stress");
    rowCount = ui->tableSigmaX->rowCount();
    for (int i = 0; i < rowCount; i++){
        xmlWriter.writeStartElement("SigmaX");
            xmlWriter.writeAttribute("x", ui->tableSigmaX->item(i,0)->text());
            xmlWriter.writeAttribute("Sigma", ui->tableSigmaX->item(i,1)->text());
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement("ExTable");
    xmlWriter.writeComment("E(epsilon) [* 10^(-6)] - deformation");
    rowCount = ui->tableEx->rowCount();
    for (int i = 0; i < rowCount; i++){
        xmlWriter.writeStartElement("Ex");
            xmlWriter.writeAttribute("x", ui->tableEx->item(i,0)->text());
            xmlWriter.writeAttribute("E", ui->tableEx->item(i,1)->text());
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement("UxTable");
    xmlWriter.writeComment("U [micro m]- displacement");
    rowCount = ui->tableUx->rowCount();
    for (int i = 0; i < rowCount; i++){
        xmlWriter.writeStartElement("Ux");
            xmlWriter.writeAttribute("x", ui->tableUx->item(i,0)->text());
            xmlWriter.writeAttribute("U", ui->tableUx->item(i,1)->text());
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
    file.close();

    QMessageBox::information(this,"Сохранено","Файл успешно сохранен.");
}
