#include "functions.h"

//Допустимы только положительные числа
QLineEdit* newLineEditPositiveNumbers(QWidget *parent)
{
    QLineEdit* value = new QLineEdit(parent);
    value->setValidator(new QRegExpValidator(QRegExp("[0-9]{1,10}(\\.{1}[0-9]{1,6}|\\.{0})")));
    return value;
}

//Допустимы как положительные, так и отрицательные числа
QLineEdit* newLineEditNumbers(QWidget *parent)
{
    QLineEdit* value = new QLineEdit(parent);
    value->setValidator(new QRegExpValidator(QRegExp("[-]?[0-9]{1,10}(\\.{1}[0-9]{1,6}|\\.{0})")));
    return value;
}

//Допустимы только натуральные числа
QLineEdit* newLineEditNaturalNumbers(QWidget *parent)
{
    QLineEdit* value = new QLineEdit(parent);
    value->setValidator(new QRegExpValidator(QRegExp("[1-9]{1}[0-9]{0,1}")));
    return value;
}


QRegExpValidator* newValidator()
{
    return new QRegExpValidator(QRegExp("[A-Za-zА-Яа-я0-9 .,-()_]{0,}"));
}
