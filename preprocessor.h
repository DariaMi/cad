#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H

#include "node.h"
#include "rod.h"
#include "formnamesession.h"
#include "formdownloadsession.h"
#include "formsavetoxml.h"
#include <vector>
#include <QMainWindow>
#include <QGraphicsScene>
#include <QWheelEvent>
#include <QList>
#include <QTableWidget>

namespace Ui {
class Preprocessor;
}

class Preprocessor : public QMainWindow
{
    Q_OBJECT

public:
    explicit Preprocessor(QWidget *parent = 0);
    ~Preprocessor();

private slots:
    void on_addRodButton_clicked();
    void on_deleteRodButton_clicked();

    void on_addNodeButton_clicked();
    void on_deleteNodeButton_clicked();

    void on_paintButton_clicked();

    void on_saveChangesButton_clicked();

    void on_actionNames_triggered();
    void on_actionDeveloper_triggered();
    void on_actionTechnicalTask_triggered();
    void on_actionExampleOfTasks_triggered();
    void on_actionFormula_triggered();

    void on_zoomInButton_clicked();
    void on_zoomOutButton_clicked();

    void on_toCalculateButton_clicked();

    void on_buttonShowA_clicked();
    void on_buttonHideA_clicked();
    void on_buttonShowB_clicked();
    void on_buttonHideB_clicked();

    void on_actionSaveSession_triggered();
    void saveSession();
    void deleteFormNameSession();

    void on_actionLoadSession_triggered();
    void downloadSession();
    void deleteFormDownloadSession();

    void on_pushButtonBuildPlots_clicked();

    void on_pushButtonShowTables_clicked();
    void on_lineEditDivider_textChanged(const QString &arg1);

    void on_pushButtonSaveToFile_clicked();
    void createXml(QString fileName);

private:
    Ui::Preprocessor *ui;
    QGraphicsScene *scene;

    std::vector<Rod> rod;
    std::vector<Node> node;
    int pillar;         //Вид опоры (от 0 до 2 включительно)
    bool isSave;
    QVector<double> delta;

    FormNameSession* formNameSession;
    FormDownloadSession* formDownloadSession;
    FormSaveToXml* formSaveToXml;

protected:
    bool isInputCorrectForRodes();
    bool isInputCorrectForNodes();

    void wheelEvent(QWheelEvent *event);

//Drawing
    void drawLeftPillar(int startPointX, int startPointY, int length);
    void drawRightPillar(int startPointX, int startPointY, int length);
    void drawQ(int startPointX, int startPointY, int length, double q);
    void drawF(int startPointX, int startPointY, double F);
    void drawText(QString text, int posX, int posY, QColor textColor = QColor(Qt::black));

//Graphics
    void drawPlots();

//Tables
    void showTables();
    void helpTo_showTables(QTableWidget* table, double item1, double item2);
};

#endif // PREPROCESSOR_H
