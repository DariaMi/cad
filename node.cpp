#include "node.h"

void Node::setForce(double force)
{
    this->force = force;
}

void Node::setNumber(unsigned int number)
{
    this->number = number;
}

double Node::getForce()
{
    return this->force;
}

unsigned int Node::getNumber()
{
    return this->number;
}

