#include "formsavetoxml.h"
#include "ui_formsavetoxml.h"
#include <QFile>
#include <QMessageBox>
#include <QString>

FormSaveToXml::FormSaveToXml(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormSaveToXml)
{
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":/Icon/Resource/Icons/CAD.png"));

    this->setModal(true);

    this->setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    this->setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);

    ui->buttonSave->setEnabled(false);

    ui->lineEditName->setValidator(new QRegExpValidator(QRegExp("([0-9]|[A-Z]|[a-z]|[А-Я]|[а-я]|_|-|.){0,100}")));
}

FormSaveToXml::~FormSaveToXml()
{
    delete ui;
}

void FormSaveToXml::on_buttonSave_clicked()
{
    this->path = ui->lineEditPath->text();
    this->name = ui->lineEditName->text();

    if (path.contains('/')){
        if (path.at(path.size() - 1) != '/'){
            path += '/';
        }
    }
    else if (path.mid(path.size()-2, 2) != "\\"){
        path += "\\";
    }

    QString fileName = this->path + this->name + ".xml";

    QFile file(fileName);
    if (file.exists()){
        QMessageBox qmb("Имя занято",
                        "Файл с таким именем уже существует.\nПерезаписать?",
                        QMessageBox::Question,
                        QMessageBox::Yes,
                        QMessageBox::No,
                        QMessageBox::NoButton);
        qmb.setButtonText(QMessageBox::Yes, "Да");
        qmb.setButtonText(QMessageBox::No, "Нет");
        qmb.setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);

        if(qmb.exec() != QMessageBox::Yes){
            return;
        }
    }

    bool isOpen = file.open(QIODevice::ReadWrite);
    if (isOpen == false){
        QMessageBox::information(this,"Ошибка","Не удалось создать файл. Проверьте корректность введенных данных.");
        return;
    }
    file.close();

    this->hide();
    ui->lineEditName->clear();
    ui->lineEditPath->clear();

    emit save(fileName);
}

void FormSaveToXml::on_buttonCancel_clicked()
{
    this->hide();
    ui->lineEditName->clear();
    ui->lineEditPath->clear();
}

void FormSaveToXml::on_lineEditName_textChanged(const QString &arg1)
{
    if (arg1 == "" || ui->lineEditPath->text() == "")
        ui->buttonSave->setEnabled(false);
    else
        ui->buttonSave->setEnabled(true);
}

void FormSaveToXml::on_lineEditPath_textChanged(const QString &arg1)
{
    if (arg1 == "" || ui->lineEditName->text() == "")
        ui->buttonSave->setEnabled(false);
    else
        ui->buttonSave->setEnabled(true);
}
