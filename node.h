#ifndef NODE_H
#define NODE_H

class Node
{
private:
    unsigned int number;
    double force;

public:
    void setForce(double force);
    void setNumber(unsigned int number);

    double getForce();
    unsigned int getNumber();
};

#endif // NODE_H
