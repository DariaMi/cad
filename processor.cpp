#include "processor.h"

Processor::Processor(const std::vector<Rod> rods, const std::vector<Node> nodes, int pillar)
{
    this->rods = rods;
    this->nodes = nodes;
    this->pillar = pillar;    

    size = rods.size()+1;

    A = new double* [size];
    for (int i = 0; i < size; i++){
        A[i] = new double [size];
    }

    delta = new double [size];
    b = new double [size];

    this->setA();
    this->setB();
    this->setDelta();
}

Processor::~Processor()
{
    for (int i = 0; i < size; i++){
        delete[] A[i];
    }
    delete[] A;
    delete[] delta;
    delete[] b;
}

void Processor::setA()
{
    for(int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
            A[i][j] = 0;

    for(int i = 0; i < size - 1; i++){
        double temp = rods.at(i).getE() * rods.at(i).getA() / rods.at(i).getL();
        A[i][i]     += temp;
        A[i+1][i]   =  -temp;
        A[i][i+1]   =  -temp;
        A[i+1][i+1] += temp;
    }

    switch(pillar){
    case 0:
        A[0][0] = 1;
        A[0][1] = 0;
        break;
    case 1:
        A[size-1][size-1] = 1;
        A[size-1][size-2] = 0;
        break;
    case 2:
        A[0][0] = 1;
        A[0][1] = 0;
        A[size-1][size-1] = 1;
        A[size-1][size-2] = 0;
        break;
    }
}

void Processor::setB()
{
    for (int i = 0; i < size; i++){
        b[i] = 0;
    }

    for (unsigned int i = 0, n = nodes.size(); i < n; i++){
        int index = nodes.at(i).getNumber() - 1;
        b[index] += ((nodes.at(i)).getForce());
    }

    for (unsigned int i = 0, n = rods.size(); i < n; i++){
        b[i] += rods.at(i).getQ() * rods.at(i).getL() * 0.5;
        b[i+1] += rods.at(i).getQ() * rods.at(i).getL() * 0.5;
    }

    if (pillar == 0 || pillar == 2){
        b[0] = 0;
    }
    if (pillar == 1 || pillar == 2){
        b[size - 1] = 0;
    }
}

void Processor::setDelta()
{
    for (int i = 0; i < size; i++){
        delta[i] = 0;
    }
}

double *Processor::getDelta()
{
    return delta;
}

double **Processor::getA()
{
    return A;
}

double *Processor::getB()
{
    return b;
}

int Processor::getSize()
{
    return size;
}

void Processor::gauss()
{
    //Создадим копии массивов A и b,
    //чтобы не затереть оригинальные значения.
    double copyA[size][size];
    for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
            copyA[i][j] = A[i][j];

    double copyb[size];
    for (int i = 0; i < size; i++)
        copyb[i] = b[i];

    //Упрощаем матрицу A для решения методом Гаусса.
    for (int i = 0; i < size; i++){
        double diagonalElement = copyA[i][i];

        if (diagonalElement != 1){
            for (int j = i; j < size; j++)
                copyA[i][j] /= diagonalElement;
            copyb[i] /= diagonalElement;
        }

        //Рассматриваем следующую строку
        if (i+1 == size)
            continue;
        else{
            double firstElement = copyA[i+1][i];

            if (firstElement == 0)
                continue;
            else if (firstElement != 1){
                for (int j = i; j < size; j++)
                    copyA[i+1][j] /= firstElement;
                copyb[i+1] /= firstElement;
            }
            //Вычитаем строки матрицы, чтобы под диагональным элементом был ноль.
            for (int j = i; j < size; j++)
                copyA[i+1][j] -= copyA[i][j];
            copyb[i+1] -= copyb[i];
        }
    }

    //Рассчитываем вектор дельта.

    //Если есть заделка справа, то перемещение там равно нулю.
    if (pillar == 1 || pillar == 2){
        delta[size-1] = 0;
    }
    else{
        delta[size-1] = copyb[size-1]; //Не делим на A[size-1][size-1], т.к. = 1
    }

    //Считаем перемещения в узлах.
    for(int i = size - 2; i >= 0; i--){
        delta[i] = copyb[i] - delta[i+1] * copyA[i][i+1]; //Не делим на A[i][i], т.к. = 1
    }

    //Если есть заделка слева, то перемещение там равно нулю.
    if(pillar == 0 || pillar == 2){
        delta[0] = 0;
    }
}

