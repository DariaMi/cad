#ifndef FORMDOWNLOADSESSION_H
#define FORMDOWNLOADSESSION_H

#include <QDialog>

namespace Ui {
class FormDownloadSession;
}

class FormDownloadSession : public QDialog
{
    Q_OBJECT

public:
    explicit FormDownloadSession(QWidget *parent = 0);
    ~FormDownloadSession();

    QString getName();

private slots:
    void on_pushButtonDownload_clicked();

    void on_pushButtonCancel_clicked();

    void on_lineEditName_textChanged(const QString &arg1);

signals:
    void download();
    void cancel();

private:
    Ui::FormDownloadSession *ui;
    QString name;
};

#endif // FORMDOWNLOADSESSION_H
