#include "preprocessor.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Preprocessor w;
    w.show();

    return a.exec();
}
