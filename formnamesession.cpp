#include "formnamesession.h"
#include "ui_formnamesession.h"
#include "functions.h"
#include <QValidator>

FormNameSession::FormNameSession(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormNameSession)
{
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":/Icon/Resource/Icons/CAD.png"));

    ui->lineEditName->setValidator(new QRegExpValidator(QRegExp("([0-9]|[A-Z]|[a-z]|[А-Я]|[а-я]|_|-|.){0,100}")));
    ui->pushButtonSave->setEnabled(false);
    this->setModal(true);
    this->setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    this->setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);
}

FormNameSession::~FormNameSession()
{
    delete ui;
}

QString FormNameSession::getName()
{
    return name;
}

void FormNameSession::on_pushButtonSave_clicked()
{
    name = ui->lineEditName->text();
    this->close();
    emit save();
}

void FormNameSession::on_pushButtonCancel_clicked()
{
    this->close();
    emit cancel();
}


void FormNameSession::on_lineEditName_textChanged(const QString &arg1)
{
    if(arg1=="")
        ui->pushButtonSave->setEnabled(false);
    else
        ui->pushButtonSave->setEnabled(true);
}
