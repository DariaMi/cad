#ifndef FORMNAMESESSION_H
#define FORMNAMESESSION_H

#include <QDialog>
#include <QString>

namespace Ui {
class FormNameSession;
}

class FormNameSession : public QDialog
{
    Q_OBJECT

public:
    explicit FormNameSession(QWidget *parent = 0);
    ~FormNameSession();

    QString getName();

private slots:
    void on_pushButtonSave_clicked();

    void on_pushButtonCancel_clicked();

    void on_lineEditName_textChanged(const QString &arg1);

signals:
    void save();
    void cancel();

private:
    Ui::FormNameSession *ui;
    QString name;
};

#endif // FORMNAMESESSION_H
