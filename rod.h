#ifndef ROD_H
#define ROD_H

class Rod
{
private:
    double A;
    double E;
    double L;
    double sigma;
    double q;

public:
    void setRod(double L, double E, double A, double sigma, double q);
    void setL(double L);
    void setE(double E);
    void setA(double A);
    void setSigma(double sigma);
    void setQ(double q);

    double getL();
    double getE();
    double getA();
    double getSigma();
    double getQ();
};

#endif // ROD_H
