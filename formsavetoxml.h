#ifndef FORMSAVETOXML_H
#define FORMSAVETOXML_H

#include <QDialog>
#include <QString>

namespace Ui {
class FormSaveToXml;
}

class FormSaveToXml : public QDialog
{
    Q_OBJECT

public:
    explicit FormSaveToXml(QWidget *parent = 0);
    ~FormSaveToXml();

signals:
    void save(QString);

private slots:
    void on_buttonSave_clicked();

    void on_buttonCancel_clicked();

    void on_lineEditName_textChanged(const QString &arg1);

    void on_lineEditPath_textChanged(const QString &arg1);

private:
    Ui::FormSaveToXml *ui;

    QString path;
    QString name;
};

#endif // FORMSAVETOXML_H
