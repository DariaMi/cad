#include "rod.h"

void Rod::setRod(double L, double E, double A, double sigma, double q){
    this->A = A;
    this->E = E;
    this->L = L;
    this->sigma = sigma;
    this->q = q;
}

void Rod::setL(double L)
{
    this->L = L;
}

void Rod::setE(double E)
{
    this->E = E;
}

void Rod::setA(double A)
{
    this->A = A;
}

void Rod::setSigma(double sigma)
{
    this->sigma = sigma;
}

void Rod::setQ(double q)
{
    this->q = q;
}

double Rod::getL()
{
    return this->L;
}

double Rod::getE()
{
    return this->E;
}

double Rod::getA()
{
    return this->A;
}

double Rod::getSigma()
{
    return this->sigma;
}

double Rod::getQ()
{
    return this->q;
}


