#include "formdownloadsession.h"
#include "ui_formdownloadsession.h"
#include "functions.h"
#include <QMessageBox>

FormDownloadSession::FormDownloadSession(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormDownloadSession)
{
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":/Icon/Resource/Icons/CAD.png"));

    ui->lineEditName->setValidator(new QRegExpValidator(QRegExp("([0-9]|[A-Z]|[a-z]|[А-Я]|[а-я]|_|-|.){0,100}")));
    ui->pushButtonDownload->setEnabled(false);
    this->setModal(true);
    this->setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    this->setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);
}

FormDownloadSession::~FormDownloadSession()
{
    delete ui;
}

QString FormDownloadSession::getName()
{
    return this->name;
}

void FormDownloadSession::on_pushButtonDownload_clicked()
{
    name = ui->lineEditName->text();
    this->close();
    emit download();
}

void FormDownloadSession::on_pushButtonCancel_clicked()
{
    this->close();
    emit cancel();
}

void FormDownloadSession::on_lineEditName_textChanged(const QString &arg1)
{
    if(arg1=="")
        ui->pushButtonDownload->setEnabled(false);
    else
        ui->pushButtonDownload->setEnabled(true);
}
